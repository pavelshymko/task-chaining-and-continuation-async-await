using NUnit.Framework;



namespace TaskChainingAndContinuationAsyncAwait.Test
{
    public class TaskChainingAndContinuationAsyncAwaitTests
    {
        int[] array = {11, 2, 34, 48, 5, 66, 79, 89, 900, 600};
        
        [Test]
        public async Task CreateArrayAsyncShouldReturnArrayOf10RandomIntegers()
        {
            var arr = await TaskContinuation.CreateArrayAsync();
            var actual = arr.Length;
            var expected = 10;
            Assert.That(actual, Is.EqualTo(expected));
        }
        
        [Test]
        public async Task MultipliesArrayAsyncShouldReturnArrayMultipliesByNumber()
        {
            
            var actual = await TaskContinuation.MultipliesArrayAsync(array, 10);
            int[] expected = {110, 20, 340, 480, 50, 660, 790, 890, 9000, 6000};
            
            Assert.That(actual, Is.EqualTo(expected));
        }
        
        [Test]
        public async Task SortArrayAsyncShouldReturnSortArrayByAscending()
        {
            var actual = await TaskContinuation.SortArrayAsync(array) ;
            Array.Sort(array);
            var expected = array;
            Assert.That(actual, Is.EqualTo(expected));
        }
        
        [Test]
        public async Task CalculateAverageAsyncShouldReturnAverageValue()
        {
            var actual = await TaskContinuation.CalculateAverageAsync(array);
            var expected = array.Average();
            
            Assert.That(actual, Is.EqualTo(expected));
        }
    }
    
}