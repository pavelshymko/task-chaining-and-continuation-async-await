﻿namespace TaskChainingAndContinuationAsyncAwait
{
    class Program
    {
        public static void Main()
        {
            double averageValue = TaskContinuation.GetAverageValueAsync().Result;
            Console.WriteLine($"Average value: {averageValue}");
        }
    }
    
    
    public class TaskContinuation {
 
        public static async Task<double> GetAverageValueAsync()
        {
            var createArray = await CreateArrayAsync();
            var multiplyArray = await MultipliesArrayAsync(createArray, 10);
            var sortArray = await SortArrayAsync(multiplyArray);
            var averageValue = await CalculateAverageAsync(sortArray); 

            return averageValue;
        }

        public static Task<int[]> CreateArrayAsync() {
            
            Random randNum = new Random();
            int[] array = Enumerable
                .Repeat(0, 10)
                .Select(i => randNum.Next(0, 10))
                .ToArray();

            Console.WriteLine("Create an array of 10 random integers:");
            foreach (var number in array)
            {
                Console.Write($"{number} ");
            }
            Console.WriteLine();
            
            return Task.FromResult(array);
        }
        
        public static Task<int[]> MultipliesArrayAsync(int[] array, int randomNumber) 
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] *= randomNumber;
            }
                
            Console.WriteLine($"Multiplies array by {randomNumber} number:");
            foreach (var num in array)
            {
                Console.Write($"{num} ");
            }
            Console.WriteLine();
                
            return Task.FromResult(array);
        }
        
        public static Task<int[]> SortArrayAsync(int[] array) 
        {
            Array.Sort(array);
                
            Console.WriteLine("Sorting array:");
            foreach (var num in array)
            {
                Console.Write($"{num} ");
            }
            Console.WriteLine();
                
            return Task.FromResult(array);
        }
        
        public static Task<double> CalculateAverageAsync(int[] array)
        {
            double averageValue = array.Average();
            Console.WriteLine($"Average value: {averageValue}");
            return Task.FromResult(averageValue);
        }
    }
}

